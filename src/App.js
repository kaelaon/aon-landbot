import React from 'react';
import {Helmet} from "react-helmet";
import Landbot from './Landbot';
import Quote from './Quote';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import './App.css';
import Navbar from './components/Navbar'
import { ThemeProvider } from 'theme-ui'
import theme from './theme'
import bgImage from './assets/photographers-bg.png';
import styled from '@emotion/styled'
import { Global, css } from "@emotion/core";
import emotionNormalize from 'emotion-normalize';
import { Styled, Flex, Box, Text } from 'theme-ui'

/** @jsx jsx */
import { jsx } from 'theme-ui'


const MainContent = styled.main`
  background: url('${bgImage}') no-repeat;
  background-size: contain;
  width: 100%;
`


class App extends React.Component {
  state = { redirect: null };
  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }
    return ( 
      <ThemeProvider theme={theme}>
      <Global styles={css`${emotionNormalize}`} />

        <Helmet>
          <title>AON – Photographers Insurance</title>
        </Helmet>
      
        <Router>
        <div
          sx={{
            display: 'flex',
            flexDirection: 'column',
            minHeight: '100vh',
          }}>
          <header
            sx={{
              width: '100%',
            }}>
              <Navbar />
          </header>
          <MainContent
            sx={{
              backgroundColor: 'midnight',
              flex: '1 1 auto',
            }}>
            <Flex>
              <Box pt="2" pb="0" sx={{ textAlign: 'center', width: '100%', }}>
                {/* <Text>Your Quote for</Text> */}
              </Box>
            </Flex>
            <Switch>
              <Route path="/quote">
                <Quote />
              </Route>
              <Route path="/">
                <Landbot sx={{ flex: '1 1 auto'}} />
              </Route>
            </Switch>
          </MainContent>
        </div>

      </Router>
    </ThemeProvider>   
    )
  }

}



export default App;
