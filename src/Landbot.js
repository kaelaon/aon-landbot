import React from 'react';
import { Redirect } from "react-router-dom";

class Landbot extends React.Component {

  state = { redirect: null };

  componentDidMount() {
    var myLandbot = new window.LandbotFrameWidget({
      container: document.getElementById('landbot'),
      index: 'https://chats.landbot.io/v2/H-753667-7B1I18MGDO1X8E3K/index.html',
    });

    myLandbot.on('getQuote',(data) => { 
        this.setState({ redirect: "/quote" });
    })
    
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }
    return <div id="landbot" className="landbotWrapper"></div>
  }

}

export default Landbot;
