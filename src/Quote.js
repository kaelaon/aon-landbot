import React from 'react';
import { Styled, Flex, Box, Container, Card, Text, Button } from 'theme-ui'
import { FaTimes } from 'react-icons/fa';

/** @jsx jsx */
import { jsx } from 'theme-ui'

class Quote extends React.Component {

  componentDidMount() {
    var myLandbot = new window.LandbotLivechat({
      index: 'https://chats.landbot.io/v2/H-753675-22K2KU8HPJ9KL0Y3/index.html',
    });
    setTimeout(function() {
      myLandbot.sendProactive({
      "message": "Hey if you have any questions please let me know.",
      "author": "Advisor",
      "avatar": "https://storage.googleapis.com/media.helloumi.com/98550/channels/5C8YFDO9TMWPIWFH6G089TT92VFNLYXN.png",
      "extra": {
        "hide_textbox": false
      }
    });
      }, 5000);
  }

  render() {
    return (
      <Container sx={{ py: '4' }}>
        <Flex
        gap={2}
        columns={[2, '2fr 1fr']}>

        <Box sx={{ flex: '2', mr: '4' }}>

          <Card p='4'>
            <Styled.h2  sx={{ mt: 3, mb: 4, }}>What's Included</Styled.h2>
            <Styled.table>
              <thead>
                <tr>
                  <th>Included</th>
                  <th>Insured</th>
                  <th>Excess</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th colSpan="3">Portable Equipment</th>
                </tr>
                <tr>
                  <td>Cameras, Laptops, Etc</td>
                  <td>$10,000,000</td>
                  <td><FaTimes sx={{ color: 'error', }} /></td>
                </tr>
                <tr>
                  <td>Cameras, Laptops, Etc</td>
                  <td>$10,000,000</td>
                  <td>$5,000,000</td>
                </tr>
                <tr>
                  <td>Cameras, Laptops, Etc</td>
                  <td>$10,000,000</td>
                  <td>$5,000,000</td>
                </tr>
                <tr>
                  <th colSpan="3">Portable Equipment</th>
                </tr>
                <tr>
                  <td>Cameras, Laptops, Etc</td>
                  <td>$10,000,000</td>
                  <td>$5,000,000</td>
                </tr>
                <tr>
                  <td>Cameras, Laptops, Etc</td>
                  <td><FaTimes sx={{ color: 'error', }} /></td>
                  <td><FaTimes sx={{ color: 'error', }} /></td>
                </tr>
                <tr>
                  <td>Cameras, Laptops, Etc</td>
                  <td>$10,000,000</td>
                  <td>$5,000,000</td>
                </tr>
              </tbody>
            </Styled.table>
          </Card>

          {/* <Card mt="4" p="4">
            <Text as="h2" mb='3'>Your Details</Text>
          </Card> */}


        </Box>
        <Box sx={{ flex: '1', maxWidth: '360px' }}>
          <Card bg='primary' color='white' p='4' sx={{ textAlign: 'center', }}>
            <Styled.h1 as='p' sx={{ mt: 3, mb: 4, }}>$860 <span  sx={{ fontSize: 4 }}>a year</span></Styled.h1>
            <Text mt="2">Policy Start Date</Text>
            <Styled.h3 sx={{ mt: 2, }}>18/03/2020</Styled.h3>
            <Button  mt="4" sx={{ width: '100%', }} variant="outlineWhite">Buy Now</Button>
            <Text mt="4">Or call us on 1800 806 584</Text>
          </Card>
        </Box>
      </Flex>


    

        


      </Container>
    );
  }
}

export default Quote;
