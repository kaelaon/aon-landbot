export default {
    breakpoints: ['40em', '52em', '64em'],
    space: [0, 4, 8, 16, 32, 64, 128, 256, 512],
    fonts: {
      body: 'Roboto, "Helvetica Neue", sans-serif',
      heading: 'Roboto, "Helvetica Neue", sans-serif',
      monospace: 'Menlo, monospace',
    },
    fontSizes: [12, 14, 16, 20, 24, 32, 48, 64, 96],
    fontWeights: {
      body: 400,
      heading: 700,
      bold: 700,
    },
    lineHeights: {
      body: 1.5,
      heading: 1.125,
    },
    colors: {
      primary: '#D60018',
      primaryDark: '#c9041b',
      teal: '#007FAA',
      blue: '#012774',
      midnight: '#001135',
      error: '#BF4C4A',
      warning: '#F0A800',
      success: '#7AB800',
      text: '#fff',
      background: '#001135',
      white: '#FFFFFF',
      gray: {
          700: '#2E2E2E',
          600: '#4C4D4F',
          500: '#696969',
          400: '#9B9B9B',
          300: '#C0C0C0',
          200: '#E4E4E4',
          100: '#F8F8F8',
      }
    },
    text: {
      heading: {
        fontFamily: 'heading',
        lineHeight: 'heading',
        fontWeight: 'heading',
      },
    },
    styles: {
      root: {
        fontFamily: 'body',
        lineHeight: 'body',
        fontWeight: 'body',
      },
      h1: {
        variant: 'text.heading',
        fontSize: 6,
      },
      h2: {
        variant: 'text.heading',
        fontSize: 5,
      },
      h3: {
        variant: 'text.heading',
        fontSize: 4,
      },
      h4: {
        variant: 'text.heading',
        fontSize: 3,
      },
      h5: {
        variant: 'text.heading',
        fontSize: 1,
      },
      h6: {
        variant: 'text.heading',
        fontSize: 0,
      },
      pre: {
        fontFamily: 'monospace',
        overflowX: 'auto',
        code: {
          color: 'inherit',
        },
      },
      code: {
        fontFamily: 'monospace',
        fontSize: 'inherit',
      },
      table: {
        width: '100%',
        borderCollapse: 'separate',
        borderSpacing: 0,
        th: {
          textAlign: 'left',
          px: 3,
          py: 3,
          textTransform: 'uppercase',
        },
        td: {
          textAlign: 'left',
          px: 3,
          py: 3,
        },
        thead: {
          th: {
            borderBottomStyle: 'solid',
          },
        },
        tbody: {
          th: {
            backgroundColor: 'gray.100',
          },
        },
      },
    },
    sizes: {
      container: '1200px',
    },
    layout: {
      container: {
        px: 4,
      }
    },
    buttons: {
      primary: {
          color: 'white',
          background: 'primary',
          borderRadius: 4,
          p: 3,
          // border: '2px solid', 
          borderColor: 'primaryDark',
          fontWeight: '400',
          boxSizing: 'border-box',
          '&:hover': {
              background: 'primary',
          }
      },
      outlineWhite: {
        color: 'white',
        borderColor: 'white',
        borderRadius: 4,
        border: '2px solid',
        p: 3,
        fontWeight: '600',
    },
    },
    cards: {
      primary: {
        padding: 2,
        borderRadius: 8,
        boxShadow: '0 0 8px rgba(0, 0, 0, 0.125)',
        background: 'white',
        color: 'gray.600',
      },
    },

  }