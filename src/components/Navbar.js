
import { ReactComponent as Logo } from '../assets/aon-logo.svg'
import styled from '@emotion/styled'
import { Link } from 'theme-ui'
import { Button } from 'theme-ui'
import Stepper from './Stepper.js'

/** @jsx jsx */
import { jsx } from 'theme-ui'


const StyledLogo = styled(Logo)`
  height: 54px;
`

export default props => (
  <header
    sx={{
      display: 'flex',
      alignItems: 'center',
      py: 2,
      px: 4,
      background: 'white',
    }}>
    <Link to='/' p='2'>
        <StyledLogo />
    </Link>
    <div sx={{ mx: 'auto' }}>
      {/* <Stepper /> */}
    </div>
    <Button >Need Help?</Button>
  </header>
)
