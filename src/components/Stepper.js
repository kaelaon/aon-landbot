
import React from 'react';
import styled from '@emotion/styled';

/** @jsx jsx */
import { jsx } from 'theme-ui';



const ProgressSteps = styled.div`
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font-family: inherit;
    vertical-align: initial;
    display: inline-flex;
    flex-direction: row;
    flex: initial;
    align-items: center;
    list-style: none;
    width: 40rem;
`


const Step = ({ labelText, ...props }) => (
    <div {...props} sx={{
        width: '24px',
        height: '24px',
        textAlign: 'center',
        fontSize: 1,
        lineHeight: '24px',
        bg: (props.active) ? 'primary' : 'gray.200',
        flex: 'initial',
        borderRadius: '100%',
        margin: '4px',
    }}>{labelText}</div>
)

const ProgressBar = ({ progress, ...props }) => (
    <div sx={{
        height: '3px',
        bg: 'gray.200',
        flex: '1',
        borderRadius: '6px',
    }}>
        <div sx={{
            height: '3px',
            bg: 'primary',
            width: `${progress}%`,
            borderRadius: '6px',
        }}></div>
    </div>
)


export default props => (
    <ProgressSteps>
        <Step active labelText="1" />
        <ProgressBar progress="75" />
        <Step labelText="2" />
        <ProgressBar progress="0" />
        <Step labelText="3" />
    </ProgressSteps>
)
